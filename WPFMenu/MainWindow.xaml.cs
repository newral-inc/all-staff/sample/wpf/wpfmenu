﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFMenu
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        //! メニュー処理を行うデリゲート
        private delegate void MenuItemDelegate(object sender, RoutedEventArgs e);

        //! メニューとメニュー処理を関連づける辞書
        private Dictionary<string, MenuItemDelegate> MenuItemDelegateDict;

        /**
         * @brief コンストラクタ
         */
        public MainWindow()
        {
            InitializeComponent();

            // メニュー項目名とメニュー処理を行うデリゲートを登録します。
            MenuItemDelegateDict = new Dictionary<string, MenuItemDelegate>();
            MenuItemDelegateDict.Add(MenuItem1.Name, MenuItem1Delegate);
            MenuItemDelegateDict.Add(MenuItem2.Name, MenuItem2Delegate);
            MenuItemDelegateDict.Add(MenuItem3.Name, MenuItem3Delegate);
        }

        /**
         * @brief メニュー項目がクリックされた時に呼び出されます。
         * 
         * @param [in] sender メニュー項目
         * @param [in] e イベント
         */
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            // メニュー項目名からメニュー処理を行うデリゲートを取得します。
            MenuItem menuItem = (MenuItem)sender;
            if (!MenuItemDelegateDict.ContainsKey(menuItem.Name)) return;
            var menuItemDelegate = MenuItemDelegateDict[menuItem.Name];

            // メニュー処理を行うデリゲートを実行します。
            menuItemDelegate(sender, e);
        }

        /**
         * @brief メニュー1の処理を行うデリゲート
         * 
         * @param [in] sender メニュー項目
         * @param [in] e イベント
         */
        private void MenuItem1Delegate(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("メニュー1");
        }

        /**
         * @brief メニュー2の処理を行うデリゲート
         * 
         * @param [in] sender メニュー項目
         * @param [in] e イベント
         */
        private void MenuItem2Delegate(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("メニュー2");
        }

        /**
         * @brief メニュー3の処理を行うデリゲート
         * 
         * @param [in] sender メニュー項目
         * @param [in] e イベント
         */
        private void MenuItem3Delegate(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("メニュー3");
        }
    }
}
